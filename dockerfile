FROM node:slim 	
WORKDIR /var/lib/jenkins/workspace/docker-dev
COPY package.json //var/lib/jenkins/workspace/docker-dev/
RUN npm install
COPY . /var/lib/jenkins/workspace/docker-dev/
CMD node server.js
EXPOSE 1337
